#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 22:01:36 2020

@author: suryanarayanasankagiri
"""
import threading
import time
import socket


class myThread (threading.Thread):
   def __init__(self, threadID, name, counter):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
   def run(self):
      print ("Starting " + self.name)
      # Get lock to synchronize threads
#      threadLock.acquire()
      print_time(self.name, self.counter, 10)
#      print(x)
      # Free lock to release next thread
#      threadLock.release()

def print_time(threadName, delay, counter):
   while counter:
      time.sleep(delay)
      print ("%s: %s" % (threadName, time.ctime(time.time())))
      message = time.ctime(time.time())
      for sock in sockets:
          sock.send(message.encode())
      counter -= 1

#threadLock = threading.Lock()
threads = []

# Create new threads
thread1 = myThread(1, "Thread-1", 1)
thread2 = myThread(2, "Thread-2", 2)

host = '127.0.0.1'
port = 5000

mySocket = socket.socket()
mySocket.connect((host,port))

mySocket2 = socket.socket()
mySocket2.connect((host,port+2))

sockets = [mySocket, mySocket2]

# Start new Threads
thread1.start()
thread2.start()

# Add threads to thread list
threads.append(thread1)
threads.append(thread2)

# Wait for all threads to complete
for t in threads:
    t.join()
for sock in sockets:
    sock.close()
print ("Exiting Main Thread")
