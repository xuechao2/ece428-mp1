import sys
import threading
import socket
from collections import defaultdict
import socketserver
import time

class ServerHandler(socketserver.BaseRequestHandler):
    def handle(self):
        file = self.request.makefile('r')
        # DOUBT: what is there in file?
        name = file.readline().strip()
        global received
        # global client_sockets
        # ASSUMPTION: this reads only one line
        print(time.time(), "-", f"{name} connected")
        for line in file:
            if line not in received and name != "node"+str(index):
                received.append(line)
                #print(received)
                #broadcast_one(line,client_sockets)
            ts, rest = line.strip().split(maxsplit=1)
            #print(name, ts, rest)
        print(time.time(), "-", f"{name} disconnected")

class listen_server(threading.Thread):
    def __init__(self, threadID, name, ip, port):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.ip = ip
        self.port = port
    def run(self):
        print("Starting " + self.name)
        with socketserver.ThreadingTCPServer((self.ip, self.port), ServerHandler) as server:
            server.serve_forever()

class broadcast(threading.Thread):
    #def __init__(self, threadID, name, client_sockets):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        #self.client_sockets = client_sockets
    def run(self):
        global received
        global other_client_sockets
        count = 0
        print("Entering broadcast thread")
        while (1):
            if received:
                for message in received[count:]:
                    #print("I'm broadcasting:", message)
                    count = count + 1
                    #for sock in self.client_sockets:
                    for sock in other_client_sockets:
                        try:
                            sock.send(message.encode())
                        except socket.error:
                            other_client_sockets.remove(sock)
        print("finished running thread")

def broadcast_one(message,client_sockets):
#def broadcast_one(message):
    #global client_sockets
    for sock in client_sockets:
        sock.send(message.encode())


# class send_client(threading.Thread):
#     def __init__(self, threadID, name, logport):
#         threading.Thread.__init__(self)
#         self.threadID = threadID
#         self.name = name
#         self.logport = logport
#     def run(self):
#         print("Starting " + self.name)
#         with socket.create_connection(('0.0.0.0',self.logport)) as sock:
#             sock.send(f"{myname}\n".encode())
#             for line in sys.stdin:
#                 sock.send(line.encode())

class generate_ledger(threading.Thread):
   def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
   def run(self):
        global received
        count_tx = 0
        print("Starting " + self.name)
        BALANCES = defaultdict(int)
        while (1):
            if received:
                for line in received[count_tx:]:
                    count_tx = count_tx + 1
                    line = line.split()
                    if len(line) == 3:
                        BALANCES[line[1]] = BALANCES[line[1]] + int(line[2])
                    if len(line) == 5:
                        if (line[1] in BALANCES) & (BALANCES[line[1]] > int(line[4])):
                            BALANCES[line[1]] = BALANCES[line[1]] - int(line[4])
                            BALANCES[line[3]] = BALANCES[line[3]] + int(line[4])
                nonzero = { key:value for (key,value) in BALANCES.items() if value != 0}
                print("BALANCE:",nonzero)
            time.sleep(5)

myname = sys.argv[1] #myname is a string of the form "node" + "x", where x is an int
index = int(myname[4])
num_of_nodes = int(sys.argv[2])
port = int(sys.argv[3])
received = []
ips = ['172.22.94.138','172.22.156.139','172.22.158.139','172.22.94.139','172.22.156.140','172.22.158.140','172.22.94.140','172.22.156.141'] #list of all possible ips; hardcoded here
print("Active Count before starting server", threading.active_count())

server = listen_server(1, "server" + str(index), ips[index-1], port) #set up a server with default IP and appropriate port
# This is a separate thread
server.start()
print("Active Count after starting server", threading.active_count())
time.sleep(10)
client_sockets = []
for i in range(num_of_nodes):
    #myname = "node"+str(i+1)
    sock = socket.create_connection((ips[i], port))
    sock.send(f"{myname}\n".encode())
    client_sockets.append(sock)
    print("Active Count after connecting", i, ":", threading.active_count())
other_client_sockets = client_sockets[0:index-1]+client_sockets[index:]

#broadcast_thread = broadcast(2, "broadcast", other_client_sockets)
broadcast_thread = broadcast(2, "broadcast")
broadcast_thread.start()
ledger_thread = generate_ledger(3,"ledger")
ledger_thread.start()
print("Active Count", threading.active_count())
print("List of threads", threading.enumerate())


for line in sys.stdin:
    if line not in received:
        received.append(line)
    #broadcast_one(line,other_client_sockets)

#    broadcast_one(line)

server.join()
print("reached end of main program")

#time.sleep(10)
#ledger.start()
