import sys
import threading
import socket
from collections import defaultdict
import socketserver
import time
from queue import Queue, PriorityQueue
import numpy as np

class ServerHandler(socketserver.BaseRequestHandler):
    def handle(self):
        global received
        global max_proposed_priority
        global max_final_priority
        global pq
        global pending_TO
        global proposed_messages_queue #list of tuples: {message: (node, priority)}
        global TO_messages#list of all final messages received by this node. Useful for R-broadcast
        global bandwidth
        global time_of_arrival_bandwidth
        file = self.request.makefile('r')
        name = file.readline().strip() #name should be of the form node1
        print(time.time(), "-", f"{name} connected")
        for line in file:
            time_of_arrival_bandwidth.append(time.time())
            bandwidth.append(len(line.encode('utf-8')))
            # print("line:", line.strip()) #only for debug
            split_line = line.split(", ")
            #thread_lock.acquire() #enter critical section
            # Each message should have at least two fields: the type of message and the message itself
            prefix = split_line[0]
            message = split_line[1]
            # Response to initial message
            if prefix == "initial":
                message = message.strip()
                if message not in received:
                	received_lock.acquire()
                    received.append(message)
                    received_lock.release()
                    max_proposed_priority_lock.acquire()
                    max_proposed_priority = max(int(max_proposed_priority), int(max_final_priority)) + 1 + 0.1*index
                    max_proposed_priority_lock.release()
                    prefixed_message = "proposed, " + message +  ", " + str(max_proposed_priority) + "\n"
<<<<<<< HEAD:node_xuechao2.py
                    name = message.split(" ")[-2]
                    proposed_messages_queue_lock.acquire()
                    proposed_messages_queue.append((name, prefixed_message))
                    proposed_messages_queue_lock.release()
                    pq_lock.acquire()
=======
                    temp_name = message.split(" ")[-2]
                    proposed_messages_queue.append((temp_name, prefixed_message))
>>>>>>> a822b41c1f0f81c1eb3ff092879abf446cdc54e7:other code/node_surya.py
                    pq.put((max_proposed_priority, 1, message)) #1 indicates message is still in proposed priority mode => undeliverable
                    pq_lock.release()
            elif prefix == "proposed":
                # print("I GOT A PROPOSED MESSAGE")
                pending_TO_lock.acquire()
                pending_TO[message].append(float(split_line[2].strip()))

                if len(pending_TO[message]) >= len(other_client_sockets) + 1:
                    final_priority = max(pending_TO[message])
                    del pending_TO[message]
                    TO_messages_lock.acquire()
                    TO_messages.append("final, " + message +  ", " + str(final_priority) + "\n")
                    TO_messages_lock.release()
                    pq_lock.acquire()
                    self.remove_from_pq(message, 1)
                    pq.put((final_priority, 2, message)) #2 indicates message is deliverable
                    pq_lock.release()
                    max_final_priority_lock.acquire()
                    max_final_priority = max(max_final_priority, final_priority)
                    max_final_priority_lock.release()
                    # should we also be sending final messages from here? No; that should have a separate broadcast thread
                pending_TO_lock.release()
            elif prefix == "final":
                # print("I GOT A FINAL MESSAGE")
                if line not in TO_messages:
                    final_priority = float(split_line[2].strip())
                    max_final_priority_lock.acquire()
                    max_final_priority = max(max_final_priority, final_priority)
                    max_final_priority_lock.release()
                    TO_messages_lock.acquire()
                    TO_messages.append(line)
                    TO_messages_lock.release()
                    pq_lock.acquire()
                    self.remove_from_pq(message, 1)
                    pq.put((final_priority, 2, message)) #2 indicates message is deliverable
                    pq_lock.release()
            #thread_lock.release() #exit critical section
            #print("received: ", received)
            #print("priority queue: ", pq.queue)
            # print(name, line.strip())
        print(time.time(), "-", f"{name} disconnected")

    def remove_from_pq(self, chosen_message, chosen_deliverable):
        temp_queue =  Queue()
        global pq
        while not pq.empty():
            priority, deliverable, message = pq.get()
            if message != chosen_message:
                temp_queue.put((priority, deliverable, message))
            else:
                if deliverable != chosen_deliverable:
                    print("ERROR IN PRIORITY QUEUE!")
                    temp_queue.put((priority, deliverable, message))
                else:
                    break
        while not temp_queue.empty():
            pq.put(temp_queue.get())
        del temp_queue

class listen_server(threading.Thread):
    def __init__(self, threadID, name, ip, port):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.ip = ip
        self.port = port
    def run(self):
        print("Starting " + self.name)
        with socketserver.ThreadingTCPServer((self.ip, self.port), ServerHandler) as server:
            server.serve_forever()

class broadcast(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
    def run(self):
        global received
        global max_proposed_priority
        global max_final_priority
        global pq
        global pending_TO
        global proposed_messages_queue #list of tuples: {message: (node, priority)}
        global TO_messages#list of all final messages received by this node. Useful for R-broadcast
        count_received = 0
        count_proposed = 0
        count_TO = 0
        print("Starting broadcast thread")
        while (1):
            # if received:
            for message in received[count_received:]:
                # print("I'm broadcasting:", message) #for debug
                count_received = count_received + 1
                prefixed_message = "initial, " + message + "\n"
                for sock in other_client_sockets:
                    try:
                        sock.send(prefixed_message.encode())
                        # print("Finished broadcasting")
                    except socket.error:
                    	socket_lock.acquire()
                        other_client_sockets.remove(sock)
                        socket_lock.release()
                        # client_sockets.remove(sock)
            for node, message in proposed_messages_queue[count_proposed:]:
                # print("I'm proposing:", message, "to ", node) #for debug
                count_proposed = count_proposed + 1
                index = int(node[-1]) - 1
                sock = client_sockets[index]
                try:
                    sock.send(message.encode())
                    # print("Finished proposing")
                except socket.error:
                    socket_lock.acquire()
                    other_client_sockets.remove(sock)
                    socket_lock.release()
                    # client_sockets.remove(sock)
            for message in TO_messages[count_TO:]:
                # print("I'm broadcasting:", message) #for debug
                count_TO = count_TO + 1
                for sock in other_client_sockets:
                    try:
                        sock.send(message.encode())
                        # print("Finished broadcasting")
                    except socket.error:
                        socket_lock.acquire()
                        other_client_sockets.remove(sock)
                        socket_lock.release()
                        # client_sockets.remove(sock)
        print("finished running thread")

#class fetch_from_pq(threading.Thread):
    #need to implement this
# class send_client(threading.Thread):
#     def __init__(self, threadID, name, logport):
#         threading.Thread.__init__(self)
#         self.threadID = threadID
#         self.name = name
#         self.logport = logport
#     def run(self):
#         print("Starting " + self.name)
#         with socket.create_connection(('0.0.0.0',self.logport)) as sock:
#             sock.send(f"{myname}\n".encode())
#             for line in sys.stdin:
#                 sock.send(line.encode())

class deliver_message(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name

    def run(self):
        global received
        global max_proposed_priority
        #global max_final_priority
        global pq
        global pending_TO
        global proposed_messages_queue #list of tuples: {message: (node, priority)}
        global TO_messages#list of all final messages received by this node. Useful for R-broadcast
        global delivered
        global delay
        print("Starting " + self.name)
        BALANCES = defaultdict(int)
        while (1):
            #thread_lock.acquire()
            #add elements to be delivered
            while not pq.empty():
                priority, deliverable, message = pq.get()
                line = message.strip().split()
                if deliverable != 2:
                    temp_name = line[-2]
                    temp_index = int(temp_name[-1])
                    if temp_index == index:
                    # check if it indeed received a "proposed" from all alive TO_messages
                        if len(pending_TO[message]) >= len(other_client_sockets) + 1:
                            final_priority = max(pending_TO[message])
                            pending_TO_lock.acquire()
                            del pending_TO[message]
                            pending_TO_lock.release()
                            TO_messages_lock.acquire()
                            TO_messages.append("final, " + message +  ", " + str(final_priority) + "\n")
                            TO_messages_lock.release()
                            pq_lock.acquire()
                            pq.put((final_priority, 2, message)) #2 indicates message is deliverable
                            pq_lock.release()
                            max_final_priority_lock.acquire()
                            max_final_priority = max(max_final_priority, final_priority)
                            max_final_priority_lock.release()
                        else:
                        	pq_lock.acquire()
                            pq.put((priority, deliverable, message))
                            pq_lock.release()
                            break
                    else:
                    # check if message's sender is still alive
                        if client_sockets[temp_index - 1] not in other_client_sockets:
                            continue
                        else:
                            pq_lock.acquire()
                            pq.put((priority, deliverable, message))
                            pq_lock.release()
                            break
                else:
                    delivered.append(message)
                    delay.append(time.time()-float(line[-1]))
<<<<<<< HEAD:node_xuechao2.py

            #thread_lock.release()
            #print(delivered)
=======
                    time_of_arrival_delay.append(time.time())
            thread_lock.release()
            print(delivered)
>>>>>>> a822b41c1f0f81c1eb3ff092879abf446cdc54e7:other code/node_surya.py
            #print(delay)
            time.sleep(1)

class generate_ledger(threading.Thread):
   def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
   def run(self):
        global delivered
        count_delivered = 0
        print("Starting " + self.name)
        BALANCES = defaultdict(int)
        while (1):
            for line in delivered[count_delivered:]:
                count_delivered = count_delivered + 1
                line = line.split()
                line = line[:-2]
                if len(line) == 3:
                    BALANCES[line[1]] = BALANCES[line[1]] + int(line[2])
                if len(line) == 5:
                    if (line[1] in BALANCES) & (BALANCES[line[1]] > int(line[4])):
                        BALANCES[line[1]] = BALANCES[line[1]] - int(line[4])
                        BALANCES[line[3]] = BALANCES[line[3]] + int(line[4])
            nonzero = { key:value for (key,value) in BALANCES.items() if value != 0}
            print("BALANCE:",nonzero)
            time.sleep(5)


myname = sys.argv[1] #myname is a string of the form "node" + "x", where x is an int
index = int(myname[4])
num_of_nodes = int(sys.argv[2])
pending_TO = {} #list of all messages received by this node from gentx, which are pending a total order
#pending_TO[message] = [list of nodes from whom we have received proposed priority message]
received = [] #list of all initial messages received by this node, including the ones from gentx.py. Useful for R-broadcast
proposed_messages_queue = [] #list of tuples: {message: (node, priority)}
TO_messages = [] #list of all final messages received by this node. Useful for R-broadcast
pq = PriorityQueue() #for ISIS algo only
max_final_priority = 0 #useful for ISIS algo to propose new priority
max_proposed_priority = 0 #useful for ISIS algo to propose new priority
delivered = []
delay = []
<<<<<<< HEAD:node_xuechao2.py

received_lock = threading.Lock()
pending_TO_lock = threading.Lock()
proposed_messages_queue_lock = threading.Lock()
TO_messages_lock = threading.Lock()
pq_lock = threading.Lock()
max_final_priority_lock = threading.Lock()
max_proposed_priority_lock = threading.Lock()
socket_lock = threading.Lock()

=======
time_of_arrival_delay = []
bandwidth = []
time_of_arrival_bandwidth = []
thread_lock = threading.Lock()
>>>>>>> a822b41c1f0f81c1eb3ff092879abf446cdc54e7:other code/node_surya.py
port = int(sys.argv[3])
#ip = '127.0.0.1'
ips = ['172.22.94.138','172.22.156.139','172.22.158.139','172.22.94.139','172.22.156.140','172.22.158.140','172.22.94.140','172.22.156.141'] #list of all possible ips; hardcoded here
#ports = [5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007]
if len(sys.argv) > 4:
    total_time = int(sys.argv[4])
else:
    total_time = 0
server = listen_server(1, "server" + str(index), ips[index-1], port)
server.start()
time.sleep(10)
client_sockets = []
all_nodes = [] #used to compute other_nodes
for i in range(num_of_nodes):
    sock = socket.create_connection((ips[i], port))
    sock.send(f"{myname}\n".encode())
    client_sockets.append(sock)
    all_nodes.append("node" + str(i+1))

time.sleep(1)
# print("Active Count after connecting to", num_of_nodes ,"clients:", threading.active_count())
# print("List of threads", threading.enumerate())
other_client_sockets = client_sockets[0:index-1]+client_sockets[index:]
other_nodes = all_nodes[0:index-1] + all_nodes[index:]
#print("No. of other nodes:", len(other_nodes), len(other_client_sockets))
#^^used to to keep track of nodes from which one has heard proposed priorities
#^^see comment on pending_TO above
start_time = time.time()
broadcast_thread = broadcast(2, "broadcast")
broadcast_thread.start()
deliver_thread = deliver_message(3,"ledger")
deliver_thread.start()
ledger_thread = generate_ledger(4,"ledger")
ledger_thread.start()
# print("Active Count", threading.active_count())
# print("List of threads", threading.enumerate())


for line in sys.stdin:
<<<<<<< HEAD:node_xuechao2.py
    time_stamp = str(time.time())
    #thread_lock.acquire()
    line = line.strip() + " " + myname + " " + time_stamp
    received_lock.acquire()
=======
    time_stamp = time.time()
    if time_stamp - start_time > total_time and total_time > 0:
        break
    thread_lock.acquire()
    line = line.strip() + " " + myname + " " + str(time_stamp)
>>>>>>> a822b41c1f0f81c1eb3ff092879abf446cdc54e7:other code/node_surya.py
    received.append(line)
    received_lock.release()
    max_proposed_priority_lock.acquire()
    max_proposed_priority = max(int(max_proposed_priority), int(max_final_priority)) + 1 + 0.1*index
    max_proposed_priority_lock.release()
    pq_lock.acquire()
    pq.put((max_proposed_priority, 1, line))
    pq_lock.release()
    pending_TO_lock.acquire()
    pending_TO[line] = [max_proposed_priority]
<<<<<<< HEAD:node_xuechao2.py
    pending_TO_lock.release()
    #thread_lock.release()
=======
    thread_lock.release()
np.save('delay' + myname, delay)
np.save('time_of_arrival_delay' + myname, time_of_arrival_delay)
np.save('bandwidth' + myname, bandwidth)
np.save('time_of_arrival_bandwidth' + myname, time_of_arrival_bandwidth)
>>>>>>> a822b41c1f0f81c1eb3ff092879abf446cdc54e7:other code/node_surya.py
server.join()
print("reached end of main program")

#time.sleep(10)
#ledger.start()
