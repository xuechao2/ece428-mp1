#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 16:07:57 2020

@author: suryanarayanasankagiri
"""

import socket
import sys

IP = '127.0.0.1'
PORT = 5000

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    # Connect to server and send data
    sock.connect((IP, PORT))
    # Receive data from the server and shut down
    while True:
        data = sock.recv(1024).decode()
        if not data:
            break
        print ("from connected  user: " + str(data))
