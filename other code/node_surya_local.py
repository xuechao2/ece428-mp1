import sys
import threading
import socket
from collections import defaultdict
import socketserver
import time
from queue import Queue, PriorityQueue

class ServerHandler(socketserver.BaseRequestHandler):
    def handle(self):
        global received
        global max_proposed_priority
        global max_final_priority
        global pq
        global pending_TO
        global proposed_messages_queue #list of tuples: {message: (node, priority)}
        global TO_messages#list of all final messages received by this node. Useful for R-broadcast
        file = self.request.makefile('r')
        name = file.readline().strip() #name should be of the form node1
        print(time.time(), "-", f"{name} connected")
        for line in file:
            bandwidth.append(len(line.encode('utf-8')))
            # print("line:", line.strip()) #only for debug
            split_line = line.split(", ")
            thread_lock.acquire() #enter critical section
            # Each message should have at least two fields: the type of message and the message itself
            prefix = split_line[0]
            message = split_line[1]
            # Response to initial message
            if prefix == "initial":
                message = message.strip()
                if message not in received:
                    received.append(message)
                    max_proposed_priority = max(int(max_proposed_priority), int(max_final_priority)) + 1 + 0.1*index
                    prefixed_message = "proposed, " + message +  ", " + str(max_proposed_priority) + "\n"
                    name = message.split(" ")[-2]
                    proposed_messages_queue.append((name, prefixed_message))
                    pq.put((max_proposed_priority, 1, message)) #1 indicates message is still in proposed priority mode => undeliverable
            elif prefix == "proposed":
                # print("I GOT A PROPOSED MESSAGE")
                pending_TO[message].append(float(split_line[2].strip()))
                if len(pending_TO[message]) >= len(other_client_sockets) + 1:
                    final_priority = max(pending_TO[message])
                    del pending_TO[message]
                    TO_messages.append("final, " + message +  ", " + str(final_priority) + "\n")
                    self.remove_from_pq(message, 1)
                    pq.put((final_priority, 2, message)) #2 indicates message is deliverable
                    # should we also be sending final messages from here? No; that should have a separate broadcast thread
            elif prefix == "final":
                # print("I GOT A FINAL MESSAGE")
                if line not in TO_messages:
                    final_priority = float(split_line[2].strip())
                    max_final_priority = max(max_final_priority, final_priority)
                    TO_messages.append(line)
                    self.remove_from_pq(message, 1)
                    pq.put((final_priority, 2, message)) #2 indicates message is deliverable
            thread_lock.release() #exit critical section
            #print("received: ", received)
            #print("priority queue: ", pq.queue)
            # print(name, line.strip())
        print(time.time(), "-", f"{name} disconnected")

    def remove_from_pq(self, chosen_message, chosen_deliverable):
        temp_queue =  Queue()
        global pq
        while not pq.empty():
            priority, deliverable, message = pq.get()
            if message != chosen_message:
                temp_queue.put((priority, deliverable, message))
            else:
                if deliverable != chosen_deliverable:
                    print("ERROR IN PRIORITY QUEUE!")
                    temp_queue.put((priority, deliverable, message))
                else:
                    break
        while not temp_queue.empty():
            pq.put(temp_queue.get())
        del temp_queue

class listen_server(threading.Thread):
    def __init__(self, threadID, name, ip, port):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.ip = ip
        self.port = port
    def run(self):
        print("Starting " + self.name)
        with socketserver.ThreadingTCPServer((self.ip, self.port), ServerHandler) as server:
            server.serve_forever()

class broadcast(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
    def run(self):
        global received
        global max_proposed_priority
        global max_final_priority
        global pq
        global pending_TO
        global proposed_messages_queue #list of tuples: {message: (node, priority)}
        global TO_messages#list of all final messages received by this node. Useful for R-broadcast
        count_received = 0
        count_proposed = 0
        count_TO = 0
        print("Starting broadcast thread")
        while (1):
            # if received:
            for message in received[count_received:]:
                # print("I'm broadcasting:", message) #for debug
                count_received = count_received + 1
                prefixed_message = "initial, " + message + "\n"
                for sock in other_client_sockets:
                    try:
                        sock.send(prefixed_message.encode())
                        # print("Finished broadcasting")
                    except socket.error:
                        other_client_sockets.remove(sock)
                        # client_sockets.remove(sock)
            for node, message in proposed_messages_queue[count_proposed:]:
                # print("I'm proposing:", message, "to ", node) #for debug
                count_proposed = count_proposed + 1
                index = int(node[-1]) - 1
                sock = client_sockets[index]
                try:
                    sock.send(message.encode())
                    # print("Finished proposing")
                except socket.error:
                    other_client_sockets.remove(sock)
                    # client_sockets.remove(sock)
            for message in TO_messages[count_TO:]:
                # print("I'm broadcasting:", message) #for debug
                count_TO = count_TO + 1
                for sock in other_client_sockets:
                    try:
                        sock.send(message.encode())
                        # print("Finished broadcasting")
                    except socket.error:
                        other_client_sockets.remove(sock)
                        # client_sockets.remove(sock)
        print("finished running thread")

#class fetch_from_pq(threading.Thread):
    #need to implement this
# class send_client(threading.Thread):
#     def __init__(self, threadID, name, logport):
#         threading.Thread.__init__(self)
#         self.threadID = threadID
#         self.name = name
#         self.logport = logport
#     def run(self):
#         print("Starting " + self.name)
#         with socket.create_connection(('0.0.0.0',self.logport)) as sock:
#             sock.send(f"{myname}\n".encode())
#             for line in sys.stdin:
#                 sock.send(line.encode())

class generate_ledger(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name

    # def remove_from_pq(self, chosen_message, chosen_deliverable):
    #     temp_queue =  Queue()
    #     global pq
    #     while not pq.empty():
    #         priority, deliverable, message = pq.get()
    #         if message != chosen_message:
    #             temp_queue.put((priority, deliverable, message))
    #         else:
    #             if deliverable != chosen_deliverable:
    #                 print("ERROR IN PRIORITY QUEUE!")
    #                 temp_queue.put((priority, deliverable, message))
    #             else:
    #                 break
    #     while not temp_queue.empty():
    #         pq.put(temp_queue.get())
    #     del temp_queue

    def run(self):
        global received
        global max_proposed_priority
        global max_final_priority
        global pq
        global pending_TO
        global proposed_messages_queue #list of tuples: {message: (node, priority)}
        global TO_messages#list of all final messages received by this node. Useful for R-broadcast
        print("Starting " + self.name)
        BALANCES = defaultdict(int)
        while (1):
            thread_lock.acquire()
            #add elements to be delivered
            while not pq.empty():
                priority, deliverable, message = pq.get()
                line = message.strip().split()
                if deliverable != 2:
                    temp_name = line[-2]
                    temp_index = int(temp_name[-1])
                    if temp_index == index:
                    # check if it indeed received a "proposed" from all alive TO_messages
                        if len(pending_TO[message]) >= len(other_client_sockets) + 1:
                            final_priority = max(pending_TO[message])
                            del pending_TO[message]
                            TO_messages.append("final, " + message +  ", " + str(final_priority) + "\n")
                            pq.put((final_priority, 2, message)) #2 indicates message is deliverable
                        else:
                            pq.put((priority, deliverable, message))
                            break
                    else:
                    # check if message's sender is still alive
                        if client_sockets[temp_index - 1] not in other_client_sockets:
                            continue
                        else:
                            pq.put((priority, deliverable, message))
                            break
                else:
                    line = line[:-2] #ignore node name and time stamp here
                    if len(line) == 3:
                        BALANCES[line[1]] = BALANCES[line[1]] + int(line[2])
                    if len(line) == 5:
                        if (line[1] in BALANCES) & (BALANCES[line[1]] > int(line[4])):
                            BALANCES[line[1]] = BALANCES[line[1]] - int(line[4])
                            BALANCES[line[3]] = BALANCES[line[3]] + int(line[4])

            thread_lock.release()
            nonzero = { key:value for (key,value) in BALANCES.items() if value != 0}
            print("BALANCES:\n",nonzero)
            time.sleep(5)

def broadcast_one(message,client_sockets):
    for sock in client_sockets:
        sock.send(message.encode())

myname = sys.argv[1] #myname is a string of the form "node" + "x", where x is an int
index = int(myname[4])
num_of_nodes = int(sys.argv[2])
if len(sys.argv) > 3:
    total_time = int(sys.argv[3])
else:
    total_time = 0
pending_TO = {} #list of all messages received by this node from gentx, which are pending a total order
#pending_TO[message] = [list of nodes from whom we have received proposed priority message]
received = [] #list of all initial messages received by this node, including the ones from gentx.py. Useful for R-broadcast
proposed_messages_queue = [] #list of tuples: {message: (node, priority)}
TO_messages = [] #list of all final messages received by this node. Useful for R-broadcast
pq = PriorityQueue() #for ISIS algo only
max_final_priority = 0 #useful for ISIS algo to propose new priority
max_proposed_priority = 0 #useful for ISIS algo to propose new priority
thread_lock = threading.Lock()
#port = int(sys.argv[3])
ip = '127.0.0.1'
#ips = ['172.22.94.138','172.22.156.139','172.22.158.139','172.22.94.139','172.22.156.140','172.22.158.140','172.22.94.140','172.22.156.141'] #list of all possible ips; hardcoded here
ports = [5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007]
server = listen_server(1, "server" + str(index), ip, ports[index-1])
server.start()
time.sleep(10)
client_sockets = []
all_nodes = [] #used to compute other_nodes

for i in range(num_of_nodes):
    sock = socket.create_connection((ip, ports[i]))
    sock.send(f"{myname}\n".encode())
    client_sockets.append(sock)
    all_nodes.append("node" + str(i+1))


# print("Active Count after connecting to", num_of_nodes ,"clients:", threading.active_count())
# print("List of threads", threading.enumerate())
other_client_sockets = client_sockets[0:index-1]+client_sockets[index:]
other_nodes = all_nodes[0:index-1] + all_nodes[index:]
print("No. of other nodes:", len(other_nodes), len(other_client_sockets))
#^^used to to keep track of nodes from which one has heard proposed priorities
#^^see comment on pending_TO above

bandwidth = []
time_of_arrival = []
delays = []
start_time = time.time()
broadcast_thread = broadcast(2, "broadcast")
broadcast_thread.start()
ledger_thread = generate_ledger(3,"ledger")
ledger_thread.start()
# print("Active Count", threading.active_count())
# print("List of threads", threading.enumerate())


for line in sys.stdin:
    time_stamp = time.time()
    if time_stamp - start_time > total_time and total_time > 0:
        break
    thread_lock.acquire()
    line = line.strip() + " " + myname + " " + str(time_stamp)
    received.append(line)
    max_proposed_priority = max(int(max_proposed_priority), int(max_final_priority)) + 1 + 0.1*index
    pq.put((max_proposed_priority, 1, line))
    pending_TO[line] = [max_proposed_priority]
    thread_lock.release()
server.join()
print("reached end of main program")

#time.sleep(10)
#ledger.start()
