#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 16:47:02 2020

@author: suryanarayanasankagiri
"""

import sys
import time
i = 0

for line in sys.stdin:
    print("Line", i, ":", time.time())
    i = i + 1;
    if 'Exit' == line.rstrip():
        break
    print(f'Processing Message from sys.stdin *****{line}*****\n')
print("Done")