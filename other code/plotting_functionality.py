#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 22:23:43 2020

@author: suryanarayanasankagiri
"""

import numpy as np
import matplotlib.pyplot as plt

num_nodes = 8
failure = '_failure'
Failure = ' -Failure'
delay = []
time_of_arrival_delay = []
bandwidth = []
time_of_arrival_bandwidth = []

# fetch all the data
for i in range(num_nodes):
    delay.append(np.load('delaynode' + str(i+1) + '.npy'))
    time_of_arrival_delay.append(np.load('time_of_arrival_delaynode' + str(i+1) + '.npy'))
    bandwidth.append(np.load('bandwidthnode' + str(i+1) + '.npy'))
    time_of_arrival_bandwidth.append(np.load('time_of_arrival_bandwidthnode' + str(i+1) + '.npy'))

num_txs = max(len(delay[i]) for i in range(num_nodes))
max_delay = []
min_delay = []
for t in range(num_txs):
    delays_for_t = []
    for i in range(num_nodes):
        if len(delay[i]) > t:
            delays_for_t.append(delay[i][t])
    min_delay.append(min(delays_for_t))
    max_delay.append(max(delays_for_t))
        
plt.figure()
plt.plot(np.array(min_delay)*1000, label = "Min Delay")
plt.plot(np.array(max_delay)*1000, label = "Max Delay")
plt.legend()
plt.xlabel("Transaction Number")
plt.ylabel("Delay (ms)")
plt.title("Delay Statistics: Nodes-"+str(num_nodes) + Failure)
plt.xticks()
plt.yticks()
plt.tight_layout()
plt.savefig('delay_statistics_' + str(num_nodes) + failure + '.png')

start_time_bandwidth = min(time_of_arrival_bandwidth[i][0] for i in range(num_nodes)) - 1
end_time_bandwidth = max(time_of_arrival_bandwidth[i][-1] for i in range(num_nodes)) + 1
time_range = np.arange(start_time_bandwidth, end_time_bandwidth)
average_bandwidth = []

sum_bandwidths = []
for i in range(num_nodes):
    sum_bandwidths.append([])
    for t in time_range:
        indices = []
        indices.append(np.where((time_of_arrival_bandwidth[i] > t)*(time_of_arrival_bandwidth[i] < t + 1))[0])
        sum_bandwidths[i].append(np.sum(bandwidth[i][indices[-1]]))

plt.figure()
for i in range(num_nodes):
    plt.plot(time_range - start_time_bandwidth, np.array(sum_bandwidths[i]), label = "node" + str(i+1))
plt.legend()
plt.xlabel("Time (s)")
plt.ylabel("bandwidth (bps)")
plt.title("Bandwidth Statistics: Nodes-"+str(num_nodes) + Failure)
plt.xticks()
plt.yticks()
plt.tight_layout()
plt.savefig('bandwidth_statistics_' + str(num_nodes) + failure + '.png')
