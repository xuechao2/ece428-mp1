#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 15:55:06 2020

@author: suryanarayanasankagiri
"""
import sys
import threading
import socket
from collections import defaultdict
import socketserver
import time


class ServerHandler(socketserver.BaseRequestHandler):
    def handle(self):
        counter = 10
        delay = 1
        while counter:
            time.sleep(delay)
            print ("%s: %s" % (threading.current_thread(), time.ctime(time.time())))
            message = time.ctime(time.time())
            self.request.send(message.encode())
            counter -= 1

IP = '127.0.0.1'
PORT = 5000

with socketserver.ThreadingTCPServer((IP, PORT), ServerHandler) as server:
    server.serve_forever()
